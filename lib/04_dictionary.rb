class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add (element)
    if element.class == String
      @entries[element] = nil
    else
      element.each { |k, v| @entries[k] = v }
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(str)
    @entries.has_key?(str) ? true : false
  end

  def find(str)
    result_hash = Hash.new
    @entries.keys.each do |key|
      result_hash[key] = @entries[key] if key.include?(str)
    end

    result_hash
  end

  def printable
    final_string = String.new
    @entries.keys.sort.each do |key|
      final_string += "[#{key}] \"#{@entries[key]}\"\n"
    end

    final_string.chomp
  end

end
