class Book

  attr_accessor :title

  def title
    little_words = ["and", "the", "over", "in", "of", "a", "an"]
    word_array = @title.split(/\s+/)

    word_array.map!.with_index do |word, idx|
      if idx == 0 || little_words.include?(word) == false
        word.capitalize
      else
        word
      end
    end

    word_array.join(" ")
  end

end
