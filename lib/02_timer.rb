class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hrs = @seconds / 3600
    mins = @seconds / 60 - (hrs * 60)
    secs = @seconds % 60

    hrs_string = hrs > 9 ? "#{hrs}" : "0#{hrs}"
    mins_string = mins > 9 ? "#{mins}" : "0#{mins}"
    secs_string = secs > 9 ? "#{secs}" : "0#{secs}"

    "#{hrs_string}:#{mins_string}:#{secs_string}"
  end
  
end
