class Temperature

  attr_accessor :in_fahrenheit, :in_celsius

  def initialize(options={})
    defaults = {}
    @options = defaults.merge(options)
  end

  def in_fahrenheit
    if @options[:f].nil?
      (@options[:c] * 9.0 / 5) + 32
    else
      @options[:f]
    end
  end

  def in_celsius
    if @options[:c].nil?
      (@options[:f] - 32) * 5.0 / 9
    else
      @options[:c]
    end
  end

  def self.from_celsius(degrees)
    self.new(c: degrees)
  end

  def self.from_fahrenheit(degrees)
    self.new(f: degrees)
  end

end


class Celsius < Temperature

  def initialize(degrees)
    super(c: degrees)
  end

end



class Fahrenheit < Temperature

  def initialize(degrees)
    super(f: degrees)
  end
end
